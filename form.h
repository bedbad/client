#ifndef FORM_H
#define FORM_H

#include <QDialog>
#include <QDateTime>
#include <QTimer>
#include <QResizeEvent>
#include <QPushButton>
#include <QSlider>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSettings>

#include <QListWidget>

#include "scene.h"
#include "capture.h"
#include "buffer.h"
#include "keyboard.h"
#include "focusedit.h"
#include "filterlist.h"
#include "apihandler.h"

enum StateApp {
    StateIntro,
    StateRecGetReady,
    StateRec,
    StateReplay,
    StateText
};

namespace Ui { class Form; }

class Form : public QDialog
{
    Q_OBJECT

public:
    explicit Form(QImage imageIntro, QImage imageOverlay, QString sceneMode, APIHandler* handler, QSettings* settings, QWidget * parent = 0);
    ~Form();

    QPushButton * buttonRecord;
    QPushButton * buttonMail;
    QSlider * slider;

    FocusEdit * focusEdit;
    Keyboard * keyboard;

    StateApp state;

    QSettings * settings;

    static const QEvent::Type timerResetType = static_cast<QEvent::Type>(2000);

public slots:
    void showFrame();

    void startFrameQuery();
    void endFrameQuery();

    void onCaptureOpened();
    void onCaptureEnded();
    void onCapturePaused();
    void onRecordClicked();

    void onMailClicked();
    void onMailEntered();

    void onRecTimeout();

    void onApiTimeout();
    void onReplyFinished();
    void onIntroImageFinished();
    void onOverlayImageFinished();

    void onSceneClicked();
    void onExitTimeout();

    void onFilterChanged();

protected:
    void resizeEvent();
    void customEvent(QEvent* event);

private:

    Ui::Form * ui;

    Scene * m_scene;

    Buffer<Frame> * m_buffer;
    Capture * m_capture;

    QThread * m_frameThread;
    QTimer * m_frameTimer;

    QTimer * m_recTimer;
    int m_nRecTimer;

    QThread * m_apiThread;
    QTimer * m_apiTimer;
    QNetworkReply * m_reply;

    QImage imageIntro, imageOverlay;
    QString imageIntroPath, imageOverlayPath;

    int m_nExit;
    QTimer * m_exitTimer;

    FilterList* m_filterList;

    APIHandler* m_apiHandler;
};

#endif // FORM_H
