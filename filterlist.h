#ifndef FILTERLIST_H
#define FILTERLIST_H

#include <QListWidget>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>

#include "filter.h"
#include "apihandler.h"

#include <vector>
#include <memory>

class FilterList : public QListWidget
{
public:
    FilterList(APIHandler* handler, QWidget* parent);

    void update(APIHandler* handler);
};

#endif // FILTERLIST_H
