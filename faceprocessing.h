#ifndef FACEPROCESSING_H
#define FACEPROCESSING_H


#include <dlib/image_processing.h>
#include <dlib/opencv.h>

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>


#include <face_detection.h>

#include "filter.h"

#include <exception>
#include <initializer_list>

static const unsigned KEY_COUNT = 6;
static const unsigned KEY_LANDMARKS[KEY_COUNT] = {30,8,36,45,48,54};
static const unsigned CHUNK_SIZE = 15;//Percentage of the width of the face to extract for tracking at each keypoint

class ModelData{
public:
    ModelData(std::string path);

    cv::Point3f& get(int index);
    inline cv::Point3f& operator[] (int index) { return get(index); }

    std::vector<cv::Point3f> getKeyMat();
    inline std::vector<cv::Point3f> operator() () { return getKeyMat(); }

private:
    void load(std::string path);

    std::vector<cv::Point3f> data;
};

/**
*@brief A simple struct containing pose information, and the location of the eyes.
*/
struct Face{
    cv::Mat rotation;//!< Rotation. See Pose for more details.
    cv::Mat translation;//!< Translation. See Pose for more details.

    cv::Point2f keypoints[KEY_COUNT];//!< The location of each eye: left=0, right=1.

    bool isValid = false;//!< True if the face is valid.
};

/**
* @brief A simple struct for holding information about the location and orientation of a face.
* Uses the same format as OpenCV's solvePnP()
*/
struct Pose{
    cv::Mat translation;//!< Defines the offset of the point from the camera origin, in the coordinate system defined by the rotation.
    cv::Mat rotation;	//!< The rotation of the camera coordinates, as a euler rotation

    Pose(): translation(3,1,CV_64F), rotation(3,1,CV_64F) {}//!< Default constructor
};

class FaceProcessor;

/**
* @brief A struct containing information about a detected object. Handles tracking and pose approximation.
*/
struct Object{
    Object();
	
	/**
	@brief Creates a new object using information from the detector, including bounding box, confidence, and other information.
	@param[in] location		The bounding box of the detected object, in the provided image.
	@param[in] img			The scaled image that was used for the detection, used to initialize face tracking.
	@param[in] frameTime	The approximate time between each frame in seconds. Used for initialization of the Kalman Filter
	@param[in] confidence	The confidence value returned by the detector.
	@param[in] pyrScale		The image scale produced by cv::pyrDown(), a power of 2.
	*/
    Object(cv::Rect location, cv::Mat& img, double frameTime, float confidence = 50, int pyrScale = 1);

	/**
	@brief Updates the internal trackers with a new image, updating the pose.
	@param[in] img	The new image to use for tracking, at the same scale as the initial image.
	@return True if the tracker's succeeded in updating the pose.
	*/
    bool predict(FaceProcessor& faceProcessor, cv::Mat& img);
	
	/**
	@return The current level of confidence.
	*/
    const float& getConfidence(){
        return confidence;
    }
	
	/**
	@return The current location, scaled up by the provided pyrScale to match the unscaled image.
	*/
    cv::Rect2d getLocation();
	
	/**
	@brief Merges this object with a newly detected one, taking it's location and providing a boost in confidence.
	@param[in] img 		The new image to use for tracking.
	@param[in] newObj	The new object to merge with.
	*/
    void update(cv::Mat& img, Object newObj);
    
	/**
	@brief Performs face alignment using the full object detector provided by the FaceProcessor
	@param[in] faceProcessor	The face processor
	@param[in] img				The new image to use for tracking.
	*/
	void updateFace(FaceProcessor& faceProcessor, cv::Mat& img);
	
	/**
	@brief Detects if this object overlaps with another, usually to determine if they should be merged.
	@param[in] other	The other object to check for overlap with.
	@return True if the two objects overlap.
	*/
    bool checkIntersect(Object& other);
	
	/**
	@brief Sets an internal flag indicating the object has been merged with another (and is marked for destruction).
	*/
    void merge(){
        merged = true;
    }
	
	/**
	@return True if the object can still merge with others and is not marked for destruction.
	*/
    bool canMerge(){
        return !merged;
    }
	
	/**
	@return True if updateFace() has been called on the object and its pose is valid
	*/
    bool hasFace(){
        return face.isValid;
    }
	
	/**
	@brief Finalizes the approximation of the object's pose, updating the Kalman Filter. Must be called sometime before getPose()
	*/
    void updatePose();
	
	/**
	@return The current pose of the object.
	*/
    const Pose& getPose(){
        return pose;
    }

//private:
    void initTracker(cv::Mat& img);//!< Initializes the tracker for the outside bounding box.
    void initKeyTracker(cv::Mat& img);
    void initKalmanFilter(double frameTime);//!< Initializes the Kalman Filter.

    void correctRotation();

    float confidence;//!< The current confidence in the object's validity, generally decreasing.
    cv::Rect2d location;//!< The current location of the object (scaled down)

    int pyrScale;//!< The scale factor of the provided images and rects

    cv::Ptr<cv::Tracker> tracker;

    cv::MultiTracker* keyTracker;
    cv::KalmanFilter kFilter;//!< The Kalman Filter used to smooth the face pose.

    static constexpr float CONFIDENCE_DELTA = 1;//!< The decrease in confidence every frame.
    static const int CONFIDENCE_MERGE_BOOST = 20;//!< The boost in confidence caused by merging with another object.

    Face face;//!< The position of the detected face.
    Pose pose;//!< The filtered pose.

    bool merged;
    bool updated;
    bool err = false;
};

/**
*@brief Singleton class handling face detection, pose approximation, and filter application
*/
class FaceProcessor{
friend void Object::updateFace(FaceProcessor& faceProcessor, cv::Mat &img);
friend bool Object::predict(FaceProcessor &faceProcessor, cv::Mat &img);

public:
	/**
	@brief Initializes the face processor.
	@param[in] detector_path	The path to the seeta face detector model file (.bin).
	@param[in] predictor_path	The path to the dlib face predictor model file (.dat).
	@param[in] frameTime		The approximate time between frames (passed through to each Object's constructor to initialize the Kalman Filter).
	*/
    FaceProcessor(std::string detector_path, std::string predictor_path, std::string model_path, double frameTime);
	
	/**
	@brief Updates the face processor with a new frame.
	@param[in]  input			The new frame.
	@param[in]  findNew			Indicates whether to re-run the detector to find new faces.
	@param[in]  updateFacePoses	Indicates whether to re-run the predcitor in order to realign face poses.
	@param[out] output			An array of poses for every currently tracked object.
	*/
    void Update(cv::Mat& input, bool findNew, bool updateFacePoses, std::vector<Pose>& output);
	
	/**
	@brief Applies the filter to the image at the given pose.
	@param[in] frame	The frame to apply the filter to.
	@param[in] pose		The Pose indicating the position of the filter.
    @param[in] filter	A pointer to the Filter resource to apply
	@param[in] index	The frame index to use with the filter
	*/
    static void ApplyFilter(cv::Mat& frame, Pose& pose, Filter* filters, int index);
	
	/**
	@brief Resets the face processor, removing stored faces.
	*/
    void clear(){
        objects.clear();
    }

protected:
    Face findFace(cv::Mat& img, dlib::rectangle& rect);//!< Finds a face within a given bbox.
    void alignFace(Face& face, int width, int height);

private:
    void detectFaces(cv::Mat& scaled, std::vector<Object>& output, int pyrScale);//!< Detects new faces within the image
    void pyramid(cv::Mat& source, cv::Mat& dest, int pyrLog);//!< Scales an image down to 1/2^pyrLog of the original size

    seeta::FaceDetection faceDetector;//!< The detector used to find new faces in the image.
    dlib::shape_predictor facePredictor;//!< The predictor used to find a face's pose in the image
    cv::Ptr<cv::CLAHE> clahe;

    std::vector<Object> objects;//!< A vector of the current objects in the scene.

    double frameTime;//!< Time between frames.

    static const int DESIRED_RESOLUTION = 360;//!< The desired resolution to scale within. At the default 360, 720p video will be scaled to 640x360, and 1920p video will be scaled to 480x270.

    ModelData faceModelData;//!< Stores the 3D model data detailing the location of various features on a generic face.

    int i = 0;
};

extern cv::Point2i constrain(cv::Point2i n, cv::Point2i max);

#endif // FACEPROCESSING_H
