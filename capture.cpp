#include "capture.h"
#include "form.h"

#include <libv4l2.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <QFile>
#include <QCoreApplication>

Capture::Capture(QObject * parent) : QThread(parent)
{
    m_isEnd = false;
    m_isRecord = false;
    m_playMode = PlayLive;

    cameraBuffer = new Buffer<Frame>();
    cameraBuffer->setCapacity(2);
    cameraBuffer->setDrop(true);

    faceEngineBuffer = new Buffer<Frame>();
    faceEngineBuffer->setCapacity(2);
    faceEngineBuffer->setDrop(true);

    m_faceengine = new FaceEngine();
    m_faceengine->setInputBuffer(cameraBuffer);
    m_faceengine->setOutputBuffer(faceEngineBuffer);
}

void Capture::run()
{
    if(m_playMode == PlayLive)
    {
        // code piece to fix webcam frame rate
        // there is no opencv wrapper for this

        QString camStr = "/dev/video" + QString::number(m_settings->value("cam_num").toInt());
        {
            int d = v4l2_open(camStr.toLatin1(), O_RDWR);
            v4l2_control c;
            c.id = V4L2_CID_EXPOSURE_AUTO_PRIORITY;
            c.value = 0;
            v4l2_ioctl(d, VIDIOC_S_CTRL, &c);
            v4l2_close(d);
        }

        m_decoder = new cv::VideoCapture(0);

        m_decoder->set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'));
        m_decoder->set(cv::CAP_PROP_FRAME_WIDTH, m_settings->value("cam_res").toSize().width());
        m_decoder->set(cv::CAP_PROP_FRAME_HEIGHT, m_settings->value("cam_res").toSize().height());

        m_decoder->set(cv::CAP_PROP_FRAME_WIDTH, 800);
        m_decoder->set(cv::CAP_PROP_FRAME_HEIGHT, 600);

        m_decoder->set(cv::CAP_PROP_FPS, m_settings->value("cam_fps").toInt());

        m_fps = m_decoder->get(cv::CAP_PROP_FPS);

 //       m_resolution.setWidth(m_decoder->get(cv::CAP_PROP_FRAME_WIDTH));
 //       m_resolution.setHeight(m_decoder->get(cv::CAP_PROP_FRAME_HEIGHT));

        m_encoder = new cv::VideoWriter();

        m_faceengine->setFPS(10);
        m_faceengine->start();

        QTime t;

        qDebug() << "live";
        qDebug() << "fps: " << m_fps;
        qDebug() << "res: " << m_resolution;

        emit opened();

        int i = 0;
        forever
        {
            i++;
            i = i % 9999;

            // decode
            do{
                //m_decoder->read(m_frame);
                m_decoder->read(double_buffer[i % 2]);

                if(m_isEnd) break;
            }while(double_buffer[i % 2].empty());

           // if(m_isEnd) break;

            // display
            Frame frame;
            frame.matrix = double_buffer[i % 2];
          //  frame.matrix = m_frame;
            frame.isValid = true;

            // record
            // todo: another thread

            if(cameraBuffer->capacity() < cameraBuffer->size())
                msleep(2 * 1000. / m_fps);
            cameraBuffer->add(frame);

            Frame processedFrame;
            do{
                processedFrame = faceEngineBuffer->take();
                if(m_isEnd)break;
            }while(!processedFrame.isValid);
            if(m_isEnd)break;

            processedFrame.image = QImage(processedFrame.matrix.data, processedFrame.matrix.cols, processedFrame.matrix.rows, QImage::Format_RGB888);
            processedFrame.pixmap = QPixmap::fromImage(processedFrame.image.rgbSwapped().scaled(QApplication::desktop()->screenGeometry().width(),
                                                                              QApplication::desktop()->screenGeometry().height(),
                                                                              Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
            if(m_isRecord) {
                if(!m_encoder->isOpened()) {
                    QDir output(QDir::homePath() + "/VideoBooth/");
                    m_outputFilename = output.absolutePath() + "/";
                    m_outputFilename += QDateTime::currentDateTime().toString("dd-MM-yy_hh-mm-ss-zzz");
                    m_outputFilename += ".avi";
                    m_encoder->open(m_outputFilename.toStdString(), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
                                    m_faceengine->getFPS(), cv::Size(m_resolution.width(), m_resolution.height()));
                    m_bufferKey.clear();
                    t.start();
                }
                m_encoder->write(processedFrame.matrix);

                m_index = t.elapsed() * m_fps / 1000.;

                qDebug() << m_index;

                processedFrame.index = m_index;

                if(processedFrame.index % 3 == 0)
                    m_bufferKey.append(processedFrame);
            }
            else {
                if(m_encoder->isOpened())
                    m_encoder->release();
                m_index = 0;
                processedFrame.index = 0;
                m_bufferKey.clear();
            }

            if(m_buffer->capacity() < cameraBuffer->size())
                msleep(2 * 1000. / m_fps);
            m_buffer->add(processedFrame);

             qDebug() << t.elapsed();
        }

        m_decoder->release();
        delete m_decoder;

        delete m_encoder;

        faceEngineBuffer->clear();
        cameraBuffer->clear();
        m_buffer->clear();

        m_faceengine->stop();
        m_faceengine->wait();

        m_isEnd = false;
        m_isRecord = false;

        emit paused();
    }
    else if(m_playMode == PlayReplay)
    {
        m_decoder = new cv::VideoCapture();
        m_decoder->open(m_outputFilename.toStdString());

        qDebug() << m_outputFilename;

        m_fps = m_decoder->get(cv::CAP_PROP_FPS);
        m_resolution.setWidth(m_decoder->get(cv::CAP_PROP_FRAME_WIDTH));
        m_resolution.setHeight(m_decoder->get(cv::CAP_PROP_FRAME_HEIGHT));

        qDebug() << "replay";
        qDebug() << "fps: " << m_fps;
        qDebug() << "res: " << m_resolution;

        emit opened();

        forever
        {
            if(m_isEnd) break;

            // QTime t; t.start();

            // decode
            if(!m_decoder->read(m_frame)) {
                m_decoder->set(cv::CAP_PROP_POS_FRAMES, 0);
                m_decoder->read(m_frame);
            }

            // display
            Frame frame;
            frame.matrix = m_frame;
            frame.image = QImage(frame.matrix.data, frame.matrix.cols, frame.matrix.rows, QImage::Format_RGB888);
            frame.pixmap = QPixmap::fromImage(frame.image.rgbSwapped().scaled(QApplication::desktop()->screenGeometry().width(),
                                                                              QApplication::desktop()->screenGeometry().height(),
                                                                              Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
            frame.index = m_decoder->get(cv::CAP_PROP_POS_FRAMES);
            frame.isValid = true;
            if(m_buffer->capacity() - 1 <= m_buffer->size())
                msleep(2 * 1000. / m_fps);
            m_buffer->add(frame);

            //msleep(1000. / m_fps);

            // qDebug() << t.elapsed();
        }

        delete m_decoder;
        m_isEnd = false;
        m_isRecord = false;

        emit ended();

    }
    else if (m_playMode == PlayBufferBoomerang) { // play buffer as boomerang
        m_buffer->clear();

        emit opened();

        int n = 0;

        forever
        {
            if(m_isEnd) break;

            for(int k = 0; k < m_bufferKey.size(); k++) {
                if(m_isEnd) break;

                if(m_buffer->capacity() - 1 <= m_buffer->size())
                    msleep(2 * 1000. / m_fps);

                int i;
                if(n % 2 == 0) i = k;
                else i = m_bufferKey.size() - k - 1;
                m_buffer->add(m_bufferKey[i]);
            }

            n++;
        }

        m_bufferKey.clear();
        m_isEnd = false;
        m_isRecord = false;

        msleep(500.);

        emit ended();
    }
    else if (m_playMode == PlayBufferStopMotion) { // play buffer as stop motion
        m_fps = m_fps / 9.;
        m_buffer->clear();

        emit opened();

        int n = 0;

        forever
        {
            if(m_isEnd) break;

            for(int k = 0; k < m_bufferKey.size(); k+=3) {
                if(m_isEnd) break;

                if(m_buffer->capacity() - 4 <= m_buffer->size())
                    msleep(2 * 1000. / m_fps);

                int i;
                if(n % 2 == 0) i = k;
                else i = m_bufferKey.size() - k - 1;
                m_buffer->add(m_bufferKey[i]);
            }

            n++;
        }

        m_bufferKey.clear();
        m_isEnd = false;
        m_isRecord = false;

        msleep(500.);

        emit ended();
    }
}
