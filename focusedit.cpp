#include "focusedit.h"
#include "form.h"

FocusEdit::FocusEdit(QWidget *parent, Keyboard * keyBoard) : QLineEdit(parent), m_keyBoard(keyBoard)
{
    // this->setEchoMode(QLineEdit::Password);
}

void FocusEdit::focusInEvent(QFocusEvent * event )
{
    if (this->m_keyBoard == NULL) // this is a promotion from design
        ((Form*)this->parent())->keyboard->focusThis(this);
    else
        this->m_keyBoard->focusThis(this); // modifies focus for keyboard
    QLineEdit::focusInEvent(event);
}
