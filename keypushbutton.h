#ifndef KEYPUSHBUTTON_H
#define KEYPUSHBUTTON_H

#define DEFAULT_BACKGROUND_BUTTON       "background-color: #202020;"
#define CHANGED_BACKGROUND_BUTTON       "background:lightgray;color:darkred;"

#define DEFAULT_STYLE_BUTTON            "color:white;font-size:20pt;border: 1px outset #020202;border-radius:6px;"
#define STYLE_FOCUS_TEXT                "background: black; color: white; border: 1px solid white; font-size:20pt;"

#define EMBEDDED_KEYBOARD               "font-size:20pt"
#define WIDTH_ZOOM_KEY                  20
#define HEIGHT_ZOOM_KEY                 WIDTH_ZOOM_KEY

#define KEY_TAB                         tr("TAB")
#define KEY_ALT                         tr("ALT")
#define KEY_CAPS                        tr("CAPS")
#define KEY_CTRL_LEFT                   tr("CTRL")
#define KEY_CUT_LEFT                    tr("cut")
#define KEY_COPY                        tr("copy")
#define KEY_PASTE                       tr("paste")
#define KEY_BACKSPACE                   tr("BACKSPACE")
#define KEY_BACKSPACE_EMBEDDED          "<---"
#define KEY_HIDECHAR                    tr("echo")
#define KEY_HIDECHAR_EMBEDDED           tr("echo")
#define KEY_CANC                        tr("DEL")
#define KEY_RETURN                      tr("RETURN")
#define KEY_SPACE                       " "
#define KEY_WIDTH_EMBEDDED              26
#define KEY_HEIGHT_EMBEDDED             22

#define IS_KEY(keyTextPressed, keyValCompare)   (keyTextPressed).contains((keyValCompare), Qt::CaseInsensitive)

#define IS_TAB(text)                    IS_KEY(text, KEY_TAB)
#define IS_ALT(text)                    IS_KEY(text, KEY_ALT)
#define IS_CAPS(text)                   IS_KEY(text, KEY_CAPS)
#define IS_CTRL_LEFT(text)              IS_KEY(text, KEY_CTRL_LEFT)
#define IS_CUT_LEFT(text)               IS_KEY(text, KEY_CUT_LEFT)
#define IS_BACK(text)                   IS_KEY(text, KEY_BACKSPACE)
#define IS_BACK_EMB(text)               IS_KEY(text, KEY_BACKSPACE_EMBEDDED)
#define IS_CANC(text)                   IS_KEY(text, KEY_CANC)
#define IS_RETURN(text)                 IS_KEY(text, KEY_RETURN)
#define IS_SPACE(text)                  IS_KEY(text, KEY_SPACE)
#define IS_PASSWORD(text)               IS_KEY(text, KEY_HIDECHAR)
#define IS_PASSWORD_EMB(text)           IS_KEY(text, KEY_HIDECHAR_EMBEDDED)
#define IS_COPY(text)                   IS_KEY(text, KEY_COPY)
#define IS_PASTE(text)                  IS_KEY(text, KEY_PASTE)
#define NO_SPECIAL_KEY(text)            ((text).length() == 1)

#include <QPushButton>

class KeyPushButton : public QPushButton
{
    Q_OBJECT

public:
    KeyPushButton(QString notTranslatedText, QWidget * parent = 0);

private slots:
    void getKeyPress(bool statusCaps);

signals:
    void pressedKey(bool statusCaps);

private:
    QWidget	* m_parent;
    QString style_embedded;
    QString m_notTranslatedText;
    int m_oldYKey;

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void changeEvent(QEvent *event);
};

#endif // KEYPUSHBUTTON_H
