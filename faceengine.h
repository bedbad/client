#ifndef FACEENGINE_H
#define FACEENGINE_H

#include <QObject>
#include <QThread>

#include "buffer.h"
#include "filter.h"
#include "faceprocessing.h"
#include <mutex>

struct Frame;

class FaceEngine : public QThread
{
    Q_OBJECT

public:
    explicit FaceEngine(QObject* parent = 0);

    void stop() {
        m_isEnd = true;
    }

    void setInputBuffer(Buffer<Frame>* buff){
        m_bufferInput = buff;
    }

    void setOutputBuffer(Buffer<Frame>* buff){
        m_bufferOutput = buff;
    }

    void clearFilterSelection();
    void addFilter(Filter* filter);

    void setFPS(double fps){
        m_fps = fps;
    }

    double getFPS(){
        return std::min(m_fps, measured_fps);
    }

    ~FaceEngine();

protected:
    void run();

private:
    bool m_isEnd;
    double m_fps;
    double measured_fps;

    Buffer<Frame>* m_bufferInput;
    Buffer<Frame>* m_bufferOutput;

    std::vector<Filter*> m_filters;
    std::mutex m_filter_mutex;

    FaceProcessor* m_faceProcessor;
};

#endif // FACEENGINE_H
