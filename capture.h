#ifndef CAPTURE_H
#define CAPTURE_H

/*
 * Capture thread
 * Decodes
 * Converts between data types for display purposes
 * Encodes
 *
 *
*/
#include <memory>

#include <QThread>
#include <QImage>
#include <QPixmap>
#include <QTime>
#include <QDir>
#include <QApplication>
#include <QDesktopWidget>
#include <QSettings>

#include <opencv/cv.hpp>

#include "buffer.h"
#include "faceengine.h"

class Form;

struct Frame {
    cv::Mat matrix;
    QImage image;
    QPixmap pixmap;

    int index;

    bool isValid;
};

enum PlayMode {
    PlayLive,
    PlayReplay,
    PlayBufferBoomerang,
    PlayBufferStopMotion
};

class Capture : public QThread
{
    Q_OBJECT

public:
    explicit Capture(QObject * parent = 0);

    PlayMode playMode() {
        return m_playMode;
    }

    void setPlayMode(const PlayMode & mode) {


        m_playMode = mode;
    }

    void setBuffer(Buffer<Frame> * buffer) {
        m_buffer = buffer;
    }

    void stop() {
        m_isEnd = 1;
    }

    void setRecord(bool isRecord) {
        m_isRecord = isRecord;
    }

    double fps() {
        return m_fps;
    }

    QString outputFilename() {
        return m_outputFilename;
    }

    QSize resolution() {
        return m_resolution;
    }

    void setSettings(QSettings * settings) {
        m_settings = settings;
    }

    void addFilter(Filter* filter){
        m_faceengine->addFilter(filter);
    }

    void clearFilterSelection(){
        m_faceengine->clearFilterSelection();
    }

protected:
    void run();

signals:
    void opened();
    void ended();
    void paused();

private:
    cv::VideoCapture * m_decoder;
    double m_fps;
    QSize m_resolution;

    PlayMode m_playMode;

    cv::Mat m_frame;

    cv::Mat double_buffer[2];

    Buffer<Frame> * m_buffer;
    QList<Frame> m_bufferKey;

    cv::VideoWriter * m_encoder;
    QString m_outputFilename;
    int m_index;

    FaceEngine* m_faceengine;

    bool m_isEnd;
    bool m_isRecord;
    bool m_resetFPS = false;

    Buffer<Frame>* cameraBuffer;
    Buffer<Frame>* faceEngineBuffer;

    QSettings * m_settings;
};

#endif // CAPTURE_H
