#ifndef APIHANDLER_H
#define APIHANDLER_H

#include <QString>
#include <QUrl>
#include <QImage>
#include <QDir>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QNetworkAccessManager>

#include <functional>

#include <opencv2/opencv.hpp>

class APIHandler
{
public:
    APIHandler(QUrl url, QDir app_home);

    void updateJSON(std::function<void(void)> callback = nullptr);

    QString saveImage(QString name, QString webRoot, bool forceRedownload = false, std::function<void(void)> callback = {});
    QString saveImage(QString url, bool forceRedownload = false, std::function<void(void)> callback = {});

    template <typename T> T loadImage(QString path);
    template <typename T> T retrieveImage(QString name, QString webRoot = "");

    QJsonArray getFilterArray(){
        return json.value("filters").toArray();
    }

    QString getValue(const char* key);
    inline QString operator[] (const char* key){ return getValue(key); }

    static QString splitURL(QString url, QString& webRoot);

private:
    QByteArray download(QUrl url, std::function<void(QNetworkReply*)> callback = {});
    QByteArray readFile(QString path);
    void writeFile(QString path, QByteArray bytes);

    QJsonObject json;
    QNetworkAccessManager* manager;

    QUrl url;

    QDir app_home;
    QDir image_dir;

    bool redownload = false;
};

//extern template cv::Mat APIHandler::retrieveImage<cv::Mat>(QString name, QString webRoot = "");
//extern template QImage  APIHandler::retrieveImage<QImage> (QString name, QString webRoot = "");

//extern template cv::Mat APIHandler::loadImage<cv::Mat>(QString path);
//extern template QImage  APIHandler::loadImage<QImage> (QString path);

#endif // APIHANDLER_H
