#include "filter.h"

#include <QDebug>
#include <QPainter>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QDir>
#include <QApplication>
#include <QDesktopWidget>

#include <sstream>

#include "apihandler.h"

using namespace std;
using namespace cv;

void StaticFilter::load(QJsonObject json, APIHandler* handler){
    targetsBackground = json.value("target").toString() == "background";

    img = handler->retrieveImage<Mat>(json.value("image_path").toString(), handler->getValue("image_root"));

    QJsonArray arr = json.value("coordinates").toArray();
    for(auto it = arr.begin(); it != arr.end(); it++){
        QJsonArray arr = it->toArray();
        coor.push_back(Point3f(arr[0].toDouble(),arr[1].toDouble(),arr[2].toDouble()));//TODO: add some error checking
    }
}

Mat& StaticFilter::getGraphic(int){
    return img;
}

vector<Point3f>& StaticFilter::getCoordinates(){
    return coor;
}

Filter* StaticFilter::scaled(cv::Size target){
    if(this->scale == target)return static_cast<Filter*>(this);

    resize(img, img, target);

    scale = target;

    return static_cast<Filter*>(this);
}

void AnimatedFilter::load(QJsonObject json, APIHandler* handler){
    QString load_type = json.value("load_type").toString();
    QString webRoot = handler->getValue("image_root");
    targetsBackground = json.value("target").toString() == "background";

    if(load_type == "explicit"){
        QJsonArray imgs = json.value("image_paths").toArray();

        n = imgs.size();
        img = new Mat[n];

        for(int i = 0; i < n; i++){
            img[i] = handler->retrieveImage<Mat>(imgs.at(i).toString(), webRoot);
        }
    }else if(load_type == "enumerated"){
        n = json.value("num_images").toInt();
        img = new Mat[n];

        QString image_prefix = json.value("image_prefix").toString();
        QString image_suffix = json.value("image_suffix").toString();

        for(int i = 0; i < n; i++){
            ostringstream oss;
            oss << image_prefix.toStdString() << i << image_suffix.toStdString();

            string s = oss.str();
            QString qs = QString::fromStdString(s);

            img[i] = handler->retrieveImage<Mat>(qs, webRoot);
        }
    }

    frameSpeed = json.value("speed").toInt();

    QJsonArray arr = json.value("coordinates").toArray();
    for(auto it = arr.begin(); it != arr.end(); it++){
        QJsonArray arr = it->toArray();
        coor.push_back(Point3f(arr[0].toDouble(),arr[1].toDouble(),arr[2].toDouble()));//TODO: add some error checking
    }
}

Mat& AnimatedFilter::getGraphic(int index){
    return img[(index / frameSpeed) % n];
}

vector<Point3f>& AnimatedFilter::getCoordinates(){
    return coor;
}

Filter* AnimatedFilter::scaled(cv::Size target){
    if(this->scale == target)return static_cast<Filter*>(this);

    for(int i = 0; i < n; i++){
        resize(img[i],img[i], target);
    }

    scale = target;

    return static_cast<Filter*>(this);
}

Filter* CreateFilter(QJsonObject json, APIHandler* handler){
    QString type = json.value("type").toString();
    Filter* ret = (type == "animated") ? static_cast<Filter*>(new AnimatedFilter(json, handler)) : static_cast<Filter*>(new StaticFilter(json, handler));

    return ret;
}

FilterWidget::FilterWidget(Filter* filter, int width, int padding, QWidget* parent): QLabel(parent){
    m_filter = std::shared_ptr<Filter>(filter);

    time.start();

    setScaledContents(true);
    setFixedSize(width, width);

    this->padding = padding;
}

void FilterWidget::paintEvent(QPaintEvent* e){
    Mat graphic = m_filter.get()->getGraphic(time.elapsed() / 16);

    QImage image(graphic.data, graphic.cols, graphic.rows, QImage::Format_RGBA8888);
    image = image.scaledToWidth(width() - padding).rgbSwapped();
    QImage final(width(), width(), QImage::Format_RGBA8888);
    final.fill(Qt::transparent);

    QPainter painter(&final);
    double shiftX = padding / 2;
    double shiftY = (final.height() - image.height()) / 2;
    painter.drawImage(QPointF(shiftX,shiftY), image);

    setPixmap(QPixmap::fromImage(final));

    QLabel::paintEvent(e);
}
