#ifndef FOCUSEDIT_H
#define FOCUSEDIT_H

#include <QLineEdit>
#include "keyboard.h"

class FocusEdit : public QLineEdit
{
private:
    Keyboard * m_keyBoard;

public:
    FocusEdit(QWidget * parent = NULL, Keyboard * keyBoard = NULL);

protected:
    void focusInEvent(QFocusEvent * event);

};

#endif // FOCUSEDIT_H
