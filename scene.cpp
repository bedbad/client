#include "scene.h"

Scene::Scene(QObject * parent) : QGraphicsScene(parent)
{
    m_pixmapItem = new QGraphicsPixmapItem();
    addItem(m_pixmapItem);
    m_pixmapItem->setPos(0, 0);

    m_overlayItem = new QGraphicsPixmapItem();
    addItem(m_overlayItem);
    m_overlayItem->setPos(10, 10);

    m_textItem = new QGraphicsTextItem();
    addItem(m_textItem);
    m_textItem->setFont(QFont("Ubuntu Condensed", 72, 50));
    m_textItem->setDefaultTextColor(Qt::yellow);
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
{
    if(m_textItem->contains(event->pos()))
        emit clicked(event->pos());
}
