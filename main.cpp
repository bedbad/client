#include "form.h"
#include "apihandler.h"

#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QImage>

QSettings* settings;
APIHandler* handler;

//QString apiCall(QImage & introImage, QImage & overlayImage, QJsonDocument& apiResponse)
//{
//    QNetworkAccessManager * manager = new QNetworkAccessManager();

//    QUrl url(settings->value("api_url").toString());
//    QNetworkRequest request(url);
//    QNetworkReply * reply = manager->get(request);
//    QEventLoop loop;
//    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
//    loop.exec();

//    QByteArray json = reply->readAll();
//    QJsonDocument apiResponse = QJsonDocument::fromJson(json);

//    // overlay image
//    QUrl overlayUrl(apiResponse.object().value("overlay_url").toString());
//    request.setUrl(overlayUrl);
//    reply = manager->get(request);
//    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
//    loop.exec();
//    overlayImage = QImage::fromData(reply->readAll());

//    // intro image
//    QUrl introUrl(apiResponse.object().value("intro_url").toString());
//    request.setUrl(introUrl);
//    reply = manager->get(request);
//    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
//    loop.exec();
//    introImage = QImage::fromData(reply->readAll());

//    // intro type
//    return apiResponse.object().value("intro_type").toString();
//}

int main(int argc, char *argv[])
{
    QDir output(QDir::currentPath() + "/../connection");

    if(!output.exists()) {
        qErrnoWarning(1, "no connection data found\n");
    }


    settings = new QSettings(output.absolutePath() + "/settings.ini", QSettings::IniFormat);

    QApplication app(argc, argv);
    handler = new APIHandler(settings->value("api_url").toString(), output);

    QImage introImage = handler->retrieveImage<QImage>(handler->getValue("intro_url"));
    QImage overlayImage = handler->retrieveImage<QImage>(handler->getValue("overlay_url"));
    QString sceneMode = handler->getValue("intro_type");

    Form form(introImage, overlayImage, sceneMode, handler, settings);

    return app.exec();
}
