#include "form.h"
#include "ui_form.h"

Form::Form(QImage imageIntro, QImage imageOverlay, QString sceneMode, APIHandler* handler, QSettings* settings, QWidget * parent) : QDialog(parent), ui(new Ui::Form),
    imageIntro(imageIntro), imageOverlay(imageOverlay)
{
    ui->setupUi(this);

    setWindowFlags(Qt::FramelessWindowHint);

    state = StateIntro;

    keyboard = new Keyboard(false, 0, this);
    keyboard->setWindowFlags(Qt::FramelessWindowHint);
    keyboard->setStyleSheet("background: black;");
    keyboard->setZoomFacility(true);
    keyboard->enableSwitchingEcho(true); // enable possibility to change echo through keyboard
    keyboard->createKeyboard(); // only create keyboard
    keyboard->setVisible(false);
    connect(keyboard, SIGNAL(returnPressed()), this, SLOT(onMailEntered()));

    focusEdit = new FocusEdit(this, keyboard);
    focusEdit->setVisible(false);

    buttonRecord = new QPushButton(this);
    buttonRecord->setCheckable(true);
    buttonRecord->setStyleSheet("QPushButton { background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(232, 0, 0, 255), stop:1 rgba(226, 226, 226, 255)); "
                                "border: 1 outset gray; border-radius: 80; } "
                                "QPushButton:pressed { border: 5 inset gray; "
                                "background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(255, 113, 113, 255), stop:1 rgba(226, 226, 226, 255)); }");
    connect(buttonRecord, SIGNAL(clicked(bool)), this, SLOT(onRecordClicked()));

    buttonMail = new QPushButton(this);
    buttonMail->setStyleSheet("QPushButton { background: gray; border: 1 outset gray; border-radius: 80; padding: 10; image: url(:/resources/mail.png) stretch stretch; } "
                              "QPushButton:pressed { image: url(:/resources/mail_pressed.png); border: 5 inset gray; background: gray }");
    buttonMail->setVisible(false);
    connect(buttonMail, SIGNAL(clicked(bool)), this, SLOT(onMailClicked()));

    slider = new QSlider(this);
    slider->setOrientation(Qt::Horizontal);
    slider->setVisible(false);
    slider->setStyleSheet("QSlider::handle:horizontal { width: 0; height: 0; border: 0; } "
                          "QSlider::sub-page:horizontal { background: yellow; } "
                          "QSlider::add-page:horizontal { background: black; }");

    m_scene = new Scene();
    ui->graphicsView->viewport()->setMouseTracking(true);
    ui->graphicsView->setMouseTracking(true);
    ui->graphicsView->setScene(m_scene);
    m_scene->setText("VIDEO BOOTH");
    if(sceneMode == "LIVE") m_scene->setSceneMode(SceneVideo);
    else m_scene->setSceneMode(SceneImage);
    connect(m_scene, SIGNAL(clicked(QPointF)), this, SLOT(onSceneClicked()));

    m_buffer = new Buffer<Frame>();
    m_buffer->setCapacity(10);
    m_buffer->setDrop(true);

    m_capture = new Capture(this);
    m_capture->setBuffer(m_buffer);
    m_capture->setSettings(settings);

    m_exitTimer = new QTimer();
    connect(m_exitTimer, SIGNAL(timeout()), this, SLOT(onExitTimeout()));

    m_frameThread = new QThread();
    m_frameTimer = new QTimer();
    m_frameTimer->setTimerType(Qt::PreciseTimer);
    m_frameTimer->moveToThread(m_frameThread);
    connect(m_frameThread, SIGNAL(started()), m_frameTimer, SLOT(start()));
    connect(m_frameThread, SIGNAL(finished()), m_frameTimer, SLOT(stop()));
    connect(m_frameTimer, SIGNAL(timeout()), this, SLOT(showFrame()));

    m_apiThread = new QThread();
    m_apiTimer = new QTimer();
    m_apiTimer->setInterval(5000);
    m_apiTimer->moveToThread(m_apiThread);
    connect(m_apiThread, SIGNAL(started()), m_apiTimer, SLOT(start()));
    connect(m_apiThread, SIGNAL(finished()), m_apiTimer, SLOT(stop()));
    connect(m_apiTimer, SIGNAL(timeout()), this, SLOT(onApiTimeout()));

    m_apiHandler = handler;

    m_nRecTimer = 0;
    m_recTimer = new QTimer();
    m_recTimer->setInterval(1000);
    connect(m_recTimer, SIGNAL(timeout()), this, SLOT(onRecTimeout()));

    connect(m_capture, SIGNAL(opened()), this, SLOT(onCaptureOpened()));
    connect(m_capture, SIGNAL(ended()), this, SLOT(onCaptureEnded()));
    connect(m_capture, SIGNAL(paused()), this, SLOT(onCapturePaused()));

    m_scene->setOverlay(QPixmap());
    m_apiThread->start();

    if(m_scene->sceneMode() == SceneVideo) {
        m_capture->setPlayMode(PlayLive);
        m_capture->start();
    }
    else {
        m_scene->setPixmap(QPixmap::fromImage(imageIntro.scaled(800, 600)));
        showFullScreen();
    }

    m_filterList = new FilterList(m_apiHandler, this);
    connect(m_filterList, SIGNAL(itemSelectionChanged()), this, SLOT(onFilterChanged()));
}

Form::~Form()
{
    m_capture->stop();
    m_capture->wait();

    delete ui;
}

void Form::showFrame()
{
    Frame frame = m_buffer->take();
    if(!frame.isValid) {
        return;
    }
    m_scene->setPixmap(frame.pixmap);

    if(slider->isVisible() && m_capture->playMode() != PlayLive)
        slider->setVisible(false);
    slider->setValue(frame.index);

    // REC finish
    if(frame.index >= 6 * m_capture->fps() && m_capture->playMode() == PlayLive) {
        state = StateReplay;

        //endFrameQuery();
        m_capture->stop();
        m_capture->wait();
        m_buffer->clear();

        m_capture->setPlayMode(PlayReplay);
        m_capture->start();

        m_scene->setText("REPLAY");

        buttonMail->setVisible(true);
        m_exitTimer->start(20000);
    }
}

void Form::startFrameQuery()
{
    if(m_frameThread->isRunning())return;
    m_frameTimer->setInterval(1000. / m_capture->fps());
    m_frameThread->start();
}

void Form::endFrameQuery()
{
    m_frameThread->quit();
    m_frameThread->wait(1000);
}

void Form::onCaptureOpened()
{
    slider->setMinimum(0);
    slider->setMaximum(6 * m_capture->fps());
    startFrameQuery();

    if(!isFullScreen()) {
        showFullScreen();
    }
}

void Form::onCaptureEnded() // REPLAY FINISHED GOTO INTRO
{
    if(m_scene->sceneMode() == SceneVideo && m_capture->playMode() != PlayLive) {
        m_capture->setPlayMode(PlayLive);
        m_capture->start();
        m_filterList->setVisible(true);
    }

    buttonRecord->setChecked(false);
    if(state != StateText)buttonRecord->setVisible(true);
    slider->setVisible(false);
    buttonMail->setVisible(false);

    m_scene->setText("VIDEO BOOTH");
}

void Form::onCapturePaused()
{
    endFrameQuery();
}

void Form::onRecordClicked()
{
    buttonRecord->setVisible(false);
    m_filterList->setVisible(false);

    m_buffer->clear();
    if(!m_capture->isRunning()) {
        m_capture->setPlayMode(PlayLive);
        m_capture->start();
    }
    state = StateRecGetReady;
    m_scene->setOverlay(QPixmap::fromImage(imageOverlay));

    m_nRecTimer = 0;
    m_recTimer->start();
}

void Form::onRecTimeout()
{
    m_scene->setText("GET READY ... " + QString::number(3 - m_nRecTimer));

    m_nRecTimer++;

    if(m_nRecTimer == 4) {
        m_buffer->clear();
        state = StateRec;

        m_nRecTimer = 0;
        m_recTimer->stop();

        slider->setVisible(true);

        m_capture->setRecord(true);

        m_scene->setText("");
    }
}

void Form::onApiTimeout()
{
//    QNetworkAccessManager * manager = new QNetworkAccessManager();
//    QNetworkRequest request(url);
//    m_reply = manager->get(request);
//    connect(m_reply, SIGNAL(finished()), this, SLOT(onReplyFinished()));
//    connect(manager, SIGNAL(finished(QNetworkReply*)), manager, SLOT(deleteLater()));

    m_apiHandler->updateJSON(std::bind(&Form::onReplyFinished, this));
}

void Form::onReplyFinished()
{

//    QByteArray json = m_reply->readAll();
//    QJsonDocument document = QJsonDocument::fromJson(json);

    // overlay image
//    QUrl overlayUrl(document.object().value("overlay_url").toString());

//    QNetworkRequest request1(overlayUrl);
//    QNetworkAccessManager * manager1 = new QNetworkAccessManager();
//    QNetworkReply * reply1 = manager1->get(request1);
//    connect(reply1, SIGNAL(finished()), this, SLOT(onOverlayImageFinished()));

    //m_filterList->update(m_apiHandler);

    imageOverlayPath = m_apiHandler->saveImage(m_apiHandler->getValue("overlay_url"), false, std::bind(&Form::onOverlayImageFinished, this));

    // intro image
//    QUrl introUrl(document.object().value("intro_url").toString());
//    QNetworkRequest request2(introUrl);
//    QNetworkAccessManager * manager2 = new QNetworkAccessManager();
//    QNetworkReply * reply2 = manager2->get(request2);
//    connect(reply2, SIGNAL(finished()), this, SLOT(onIntroImageFinished()));

    imageIntroPath = m_apiHandler->saveImage(m_apiHandler->getValue("intro_url"), false, std::bind(&Form::onIntroImageFinished, this));

//    connect(manager1, SIGNAL(finished(QNetworkReply*)), manager1, SLOT(deleteLater()));
//    connect(manager2, SIGNAL(finished(QNetworkReply*)), manager2, SLOT(deleteLater()));

    // intro type
    if(m_apiHandler->getValue("intro_type") == "IMAGE") {
        if(m_scene->sceneMode() == SceneImage) return;
        m_scene->setSceneMode(SceneImage);
        if(m_capture->isRunning() && state == StateIntro) {
            endFrameQuery();
            m_capture->stop();
            m_capture->wait();
            m_buffer->clear();
        }

    }
    else {
        if(m_scene->sceneMode() == SceneVideo) return;
        m_scene->setSceneMode(SceneVideo);
        if(!m_capture->isRunning() && state == StateIntro) {
            m_capture->setPlayMode(PlayLive);
            m_capture->start();
        }
    }
}

void Form::onIntroImageFinished()
{
//    QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
//    imageIntro = QImage::fromData(reply->readAll());
    //imageIntro = m_apiHandler->loadImage<QImage>(imageIntroPath);

    if(m_scene->sceneMode() == SceneImage && state == StateIntro)
        m_scene->setPixmap(QPixmap::fromImage(imageIntro.scaled(1920, 1080)));
}

void Form::onOverlayImageFinished()
{
//    QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
//    imageOverlay = QImage::fromData(reply->readAll());

    //imageOverlay = m_apiHandler->loadImage<QImage>(imageOverlayPath);

    if(state != StateIntro)
        m_scene->setOverlay(QPixmap::fromImage(imageOverlay));
}

void Form::onSceneClicked()
{
    m_nExit++;
    m_exitTimer->stop();
    m_exitTimer->start(200);

    if(m_nExit == 5 && state == StateIntro) {
        //qApp->quit();
    }
}

void Form::onExitTimeout()
{
    m_nExit = 0;
    m_exitTimer->stop();

    if(state >= StateReplay)onMailEntered();
}

void Form::onMailClicked()
{
    state = StateText;

    buttonMail->setVisible(false);
    buttonRecord->setVisible(false);

    keyboard->show(this); // once created keyboard object, use this method to switch between windows
    keyboard->setVisible(true);
    keyboard->setGeometry(0, height() - 500, width(), 500);
    focusEdit->setGeometry(10, height() - keyboard->height() - 40, width() - 20, 40);
    focusEdit->setVisible(true);

    m_exitTimer->stop();
    m_exitTimer->start(20000);
}

void Form::onMailEntered()
{
    focusEdit->setVisible(false);
    keyboard->setVisible(false);

    QDir output(QDir::homePath() + "/VideoBooth/");
    QFile file(output.absolutePath() + "/mail.txt");
    file.open(QFile::Append);
    QTextStream stream(&file);
    stream << m_capture->outputFilename() + " " << focusEdit->text();
    stream << endl;

    focusEdit->clear();

    endFrameQuery();
    m_capture->stop();
    m_capture->wait();
    m_buffer->clear();

    state = StateIntro;
    m_scene->setOverlay(QPixmap());

    slider->setVisible(false);
    buttonMail->setVisible(false);
    buttonRecord->setVisible(true);
    m_scene->setText("VIDEO BOOTH");
}

void Form::resizeEvent()
{
    buttonRecord->setGeometry(width() * 0.25, height() * 0.75, 160, 160);
    buttonMail->setGeometry(width() * 0.65, height() * 0.75, 160, 160);
    slider->setGeometry(0, height() - 20, width(), 5);
    keyboard->setGeometry(0, height() - keyboard->height(), width(), keyboard->height());

    m_scene->setTextPos();
}

void Form::customEvent(QEvent* event){
    if(event->type() == timerResetType){
        m_frameTimer->setInterval(1000. / m_capture->fps());
    }
}


void Form::onFilterChanged()
{
    QList<QListWidgetItem*> items = m_filterList->selectedItems();

    m_capture->clearFilterSelection();

    for(int i = 0; i < items.size(); i++){
        FilterWidget* widget = dynamic_cast<FilterWidget*>(m_filterList->itemWidget(items.at(i)));
        try{
            m_capture->addFilter(widget->getFilter());
        }catch(int e){}
    }
}
