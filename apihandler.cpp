#include "apihandler.h"

#include <QEventLoop>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>

#include <QFile>
#include <QDir>

#include <functional>

using namespace cv;
using namespace std;

APIHandler::APIHandler(QUrl url, QDir app_home)
{
    manager = new QNetworkAccessManager;
    this->app_home = app_home;

    image_dir = QDir(app_home.absolutePath() + "/images");
    if(!image_dir.exists()){
        QDir().mkdir(image_dir.absolutePath());
    }

    this->url = url;
    updateJSON();
}

void APIHandler::updateJSON(function<void(void)> callback){
    auto imp = [this, callback](QNetworkReply* reply, QByteArray raw = ""){
        QByteArray bytes = "";

        if(raw.isEmpty() && reply != nullptr){
            bytes = reply->readAll();
        }else{
            bytes = raw;
        }

        QJsonDocument downloaded = QJsonDocument::fromJson(bytes);

        QString localPath = app_home.absolutePath() + "/api.json";
        QJsonDocument local = QJsonDocument::fromJson(readFile(localPath));

        if(downloaded.isEmpty()){
            if(local.isEmpty()){
                //TODO: Handle errors
            }else{
                json = local.object();
            }

            redownload = false;
        }else if(downloaded.object().value("id") != local.object().value("id")){
            writeFile(localPath, bytes);
            redownload = true;

            json = downloaded.object();
        }else{
            redownload = false;

            json = local.object();
        }

        if(callback)callback();
    };

    if(callback){
        download(url, imp);
    }else{
        imp(nullptr, download(url));
    }
}

QString APIHandler::getValue(const char* key){
    return json.value(QString(key)).toString();
}

QByteArray APIHandler::download(QUrl url, function<void(QNetworkReply*)> callback){
    QNetworkRequest request(url);
    QNetworkReply * reply = manager->get(request);

    if(callback){
        QObject::connect(manager, &QNetworkAccessManager::finished, callback);
        return QByteArray();
    }

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    return reply->readAll();
}

QByteArray APIHandler::readFile(QString path){
    QFile file(path);
    if(!file.exists())return QByteArray();

    file.open(QIODevice::ReadOnly);
    QByteArray ret = file.readAll();

    file.close();
    return ret;
}

void APIHandler::writeFile(QString path, QByteArray bytes){
    QFile file(path);
    if(file.exists()){
        file.remove();
    }

    file.open(QIODevice::WriteOnly/* | QIODevice::Truncate*/);
    file.write(bytes);
}

QString APIHandler::saveImage(QString name, QString webRoot, bool forceRedownload, function<void(void)> callback){
    QString path = image_dir.absolutePath() + "/" + name;

    if(forceRedownload || redownload || !QFile(path).exists()){
        if(callback){
            download(webRoot + name, [=](QNetworkReply* reply){
                QByteArray raw = reply->readAll();
                writeFile(path, raw);

                callback();
            });
        }else{
            QByteArray raw = download(webRoot + name);
            writeFile(path, raw);
        }
    }else{
        if(callback)callback();
    }

    return path;
}

QString APIHandler::saveImage(QString url, bool forceRedownload, function<void(void)> callback){
    QString webRoot;
    QString name = splitURL(url, webRoot);

    QString path = image_dir.absolutePath() + "/" + name;

    if(forceRedownload || redownload || !QFile(path).exists()){
        if(callback){
            download(webRoot + name, [=](QNetworkReply* reply){
                QByteArray raw = reply->readAll();
                writeFile(path, raw);

                callback();
            });
        }else{
            QByteArray raw = download(webRoot + name);
            writeFile(path, raw);
        }
    }else{
        if(callback)callback();
    }

    return path;
}

template<>
Mat APIHandler::loadImage(QString path){
    return imread(path.toStdString(), IMREAD_UNCHANGED);
}

template<>
QImage APIHandler::loadImage(QString path){
    return QImage::fromData(readFile(path));
}

template<>
Mat APIHandler::retrieveImage(QString name, QString webRoot){
    QString name_, root_;

    if(webRoot.isEmpty()){
        name_ = splitURL(name, root_);
    }else{
        name_ = name;
        root_ = webRoot;
    }

    cv::Mat ret = imread(saveImage(name_, root_).toStdString(), IMREAD_UNCHANGED);

    if(ret.channels() == 4)return ret;

    cv::Mat alpha(ret.rows,ret.cols,CV_8UC1,Scalar(255));

    cv::Mat mix[2] = {ret, alpha};

    cv::Mat fourChannel;
    cv::merge(mix, 2, fourChannel);
    return fourChannel;
}

template<>
QImage APIHandler::retrieveImage(QString name, QString webRoot){
    QString name_, root_;

    if(webRoot.isEmpty()){
        name_ = splitURL(name, root_);
    }else{
        name_ = name;
        root_ = webRoot;
    }

    return QImage::fromData(readFile(saveImage(name_, root_)));
}

QString APIHandler::splitURL(QString url, QString& webRoot){
    int i = url.size() - 1;
    for(; i >= 0; i--){
        if(url[i] == '/' || url[i] == '\\'){
            i++;
            break;
        }
    }

    webRoot = url.left(i);
    return url.right(url.size() - i);
}
