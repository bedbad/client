#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>

enum SceneMode {
    SceneImage,
    SceneVideo
};

class Scene : public QGraphicsScene
{
    Q_OBJECT

public:
    Scene(QObject * parent = 0);

    void setPixmap(const QPixmap & pixmap) {
        m_pixmapItem->setPixmap(pixmap);
    }

    void setText(const QString & text) {
        m_textItem->setPlainText(text);
        m_textItem->adjustSize();
        m_textItem->setPos(views()[0]->width() * 0.5 - m_textItem->boundingRect().width() * 0.5, 100);
    }

    void setTextPos() {
        m_textItem->setPos(views()[0]->width() * 0.5 - m_textItem->boundingRect().width() * 0.5, 100);
    }

    void setOverlay(const QPixmap & pixmap) {
        m_overlayItem->setPixmap(pixmap);
    }

    void setSceneMode(const SceneMode & mode) {
        m_sceneMode = mode;
    }
    SceneMode sceneMode() {
        return m_sceneMode;
    }

signals:
    void clicked(QPointF);

protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    QGraphicsPixmapItem * m_pixmapItem;
    QGraphicsPixmapItem * m_overlayItem;
    QGraphicsTextItem * m_textItem;

    SceneMode m_sceneMode;
};

#endif // SCENE_H
