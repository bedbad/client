#include "faceengine.h"
#include "capture.h"

#include <QDebug>
#include <QTransform>
#include <QTime>
using namespace std;





#define DETECTDATA "../train_data/seeta_fd_frontal_v1.0.bin"
#define LANDMARKDATA "../train_data/shape_predictor_68_face_landmarks.dat"
#define MODELDATA "../train_data/model_coordinates.txt"

FaceEngine::FaceEngine(QObject* parent): QThread(parent)
{
    m_isEnd = false;
    m_fps = 30;
    measured_fps = m_fps;
    m_faceProcessor = new FaceProcessor(DETECTDATA,
                                        LANDMARKDATA,
                                        MODELDATA,
                                        1./m_fps);
}

void FaceEngine::clearFilterSelection(){
    std::lock_guard<std::mutex> guard(m_filter_mutex);

    m_filters.clear();
}

void FaceEngine::addFilter(Filter* filter){
    std::lock_guard<std::mutex> guard(m_filter_mutex);

    m_filters.push_back(filter);
}

void ApplyBackgroundFilter(cv::Mat& frame, Filter* filter, int index){
    cerr << "background filter\n";
    //cv::Mat scaled = filter->scaled(cv::Size(frame.cols, frame.rows))->getGraphic(index);
    cv::Mat uncropped = filter->getGraphic(index);

    cv::Point3f coor = (filter->getCoordinates().size() > 0)? filter->getCoordinates()[0] : cv::Point3f(0,0,0);
    cv::Point2i a(coor.x, coor.y);
    cv::Point2i b = constrain(cv::Point2i(coor.x + frame.cols - 1, coor.y + frame.rows - 1), cv::Point2i(frame.cols, frame.rows));
    b = constrain(b,cv::Point2f(uncropped.cols, uncropped.rows));

    cv::Rect bounds(a,b);

    cv::Mat img = uncropped(cv::Rect(0,0,bounds.width,bounds.height));

    cv::Mat alpha(img.rows,img.cols,CV_8UC1);
    cv::Mat final(img.rows,img.cols,CV_8UC3);
    int fromTo[8] = {0,0,1,1,2,2,3,3};
    cv::Mat mix[2] = {final, alpha};
    cv::mixChannels(&img, 1, mix, 2, fromTo, 4);

    cv::Mat mask;
    cv::threshold(alpha, mask, 127, 255, cv::THRESH_BINARY);

    final.copyTo(frame(cv::Rect(a,b)), mask);
}

void FaceEngine::run(){
    unsigned int i, j;
    i = j = 0;

    unsigned int n = m_fps * 6;
    int frameTimes[n];
    int total = 0;

    std::fill(frameTimes, frameTimes+n, 0);


    forever{
        cerr<<"fps: "<<m_fps;
        if(m_isEnd)break;

        //get a frame
        Frame frame;
        do{
            frame = m_bufferInput->take();
            if(m_isEnd)break;

        }
        while(!frame.isValid);
        cerr<<"new buffered frame\n";

        if(m_isEnd)break;

        QTime t; t.start();
        //find all the faces in the frame
        vector<Pose> faces;
        m_faceProcessor->Update(frame.matrix, j % 10 == 0, j % 10 == 2, faces);

        cv::Mat final = frame.matrix;

        //apply the filter to all the faces
        for(Pose& face : faces){
            lock_guard<mutex> guard(m_filter_mutex);//temporarily deny access to the filter

            for(Filter* filter: m_filters){
                if(!filter->targetsBackground){
                    FaceProcessor::ApplyFilter(final, face, filter, i);
                }
            }
        }

        for(Filter* filter: m_filters){
            if(filter->targetsBackground){
                ApplyBackgroundFilter(final, filter, i);
            }
        }


        frame.matrix = final;

        int duration = t.elapsed();
        total += duration - frameTimes[j % n];
        frameTimes[j % n] = duration;

        measured_fps = 1000.*min(j,n)/total;

        //add the processed frame to the output buffer (wait for the buffer to clear if necessary)
        if(m_bufferOutput->capacity() < m_bufferOutput->size())
            msleep(2 * 1000. / m_fps);
        m_bufferOutput->add(frame);

        i += (60 / m_fps);//increment the frame counter (support for animated filters)
        j++;
    }


    m_faceProcessor->clear();

    m_isEnd = false;
}

FaceEngine::~FaceEngine(){
    delete m_faceProcessor;
}
