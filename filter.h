#ifndef FILTER_H
#define FILTER_H

#include <opencv2/opencv.hpp>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QLabel>
#include <QTime>

#include <memory>

#include "apihandler.h"

/**
*@brief Abstract interface for filter resources.
*/
struct Filter{
    Filter() {}
	
	/**
	@param[in] frame	The frame index indicating which graphic to retrieve (if the filter is animated)
	@return The graphic for the frame.
	*/
    virtual cv::Mat& getGraphic(int frame) = 0;
	
	/**
	@return The 3D world coordinates of the filter relative to the face model. See FaceProcessor for details.
	*/
    virtual std::vector<cv::Point3f>& getCoordinates() = 0;
	
	/**
	@brief Loads the filter data given the paths and other information specified in the QJsonObject
    @param[in] json	The JsonObject to load from. Should contain "name" and "type" (animated or static) fields
	*/
    virtual void load(QJsonObject json, APIHandler* handler) = 0;

    virtual Filter* scaled(cv::Size target) = 0;

    bool targetsBackground = false;
};

/**
*@brief A filter without animation.
*/
struct StaticFilter: Filter{
    StaticFilter(QJsonObject json, APIHandler* handler): Filter() {
        load(json, handler);
    }
	
	/**
	@brief Loads the static filter data given the paths and other information specified in the QJsonObject
    @param[in] json	The JsonObject to load from. Should contain the following fields:\n
					\li String name: The name of the filter.\n
					\li String image_path: The path to the image.\n
                    \li Array<Array<Float>>	coordinates: The 4 world coordinates corresponding to the image's corners (top left, bottom left, bottom right, top right)
	*/
    virtual void load(QJsonObject json, APIHandler* handler);
    virtual cv::Mat& getGraphic(int frame);
    virtual std::vector<cv::Point3f>& getCoordinates();
    virtual Filter* scaled(cv::Size target);

private:
    cv::Size scale;

    cv::Mat img;
    std::vector<cv::Point3f> coor;
};

/**
*@brief A filter with simple frame-by-frame animation.
*/
struct AnimatedFilter: Filter{
    AnimatedFilter(QJsonObject json, APIHandler* handler): Filter() {
        load(json, handler);
    }
	
	/**
	@brief Loads the animated filter data given the paths and other information specified in the QJsonObject
    @param[in] json	The JsonObject to load from. Should contain the following fields:\n
					\li String name: The name of the filter.\n
					\li String load_type: Determines the type used to specify the list of image paths, "enumerated" or "explicit"\n
                    \li Array<Array<Float>>	coordinates : The 4 world coordinates corresponding to the image's corners (top left, bottom left, bottom right, top right)\n
					\li Int frameSpeed: The FPS of the animation.
					### If "explicit" {\n
					\li Array<String> image_paths: An array of image paths\n
					}\n
					### If "enumerated" (This specifies a list of images: "<image_prefix><index><image_suffix>") {\n
					\li Int num_images: The number of images to load.\n
					\li String image_prefix: The first part of the image path before the index\n
					\li String image_suffix: The last part of the image path after the index\n
					}
	*/
    virtual void load(QJsonObject json, APIHandler* handler);
    virtual cv::Mat& getGraphic(int frame);
    virtual std::vector<cv::Point3f>& getCoordinates();
    virtual Filter* scaled(cv::Size target);

private:
    cv::Size scale;

    cv::Mat* img;
    std::vector<cv::Point3f> coor;
    int n;

    int frameSpeed;
};

/**
*@brief Creates a new filter from the specified object file.
*@param[in] json	The JsonObject file
*@return A pointer to the Filter resource
*/
Filter* CreateFilter(QJsonObject json, APIHandler* handler);

/**
*@brief Filter selection widget
*/
class FilterWidget: public QLabel{
public:
    FilterWidget(Filter* m_filter, int width, int padding = 20, QWidget* parent = nullptr);

    Filter* getFilter(){
        return m_filter.get();
    }

private:
    std::shared_ptr<Filter> m_filter;
    QTime time;

    void paintEvent(QPaintEvent* e);

    int padding;
};

#endif // FILTER_H
