#include "filterlist.h"

#include <QLabel>
#include <QBitmap>
#include <QDebug>
#include <QPainter>
#include <QApplication>
#include <QDesktopWidget>

FilterList::FilterList(APIHandler* handler, QWidget* parent): QListWidget(parent){
    setFixedHeight(QApplication::desktop()->screenGeometry().height());
    setFixedWidth(QApplication::desktop()->screenGeometry().width() * .08);
    setStyleSheet("QListWidget {background: rgba(0,0,0,.1)}"
                  "QListWidget:item:selected {background: rgba(94,186,33,.5)}");

    update(handler);

    setSelectionMode(QAbstractItemView::MultiSelection);
}

void FilterList::update(APIHandler* handler){
    clear();
    for(int i = 0; i < handler->getFilterArray().size(); i++){
        QListWidgetItem* item = new QListWidgetItem(this);
        addItem(item);

        QSize s = item->sizeHint();
        s.setHeight(width());
        item->setSizeHint(s);
        item->setTextAlignment(Qt::AlignCenter);

        FilterWidget* widget = new FilterWidget(CreateFilter(handler->getFilterArray()[i].toObject(), handler), width());
        setItemWidget(item, widget);
    }
}
