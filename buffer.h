#ifndef BUFFER_H
#define BUFFER_H

/**
* Buffer that is mostly used to hold frame objects
* Maximum size specified with capacity
* Thread-safe
*
*/

#define MIN_FPS 5

#include <QQueue>
#include <QSemaphore>
#include <QDebug>

template <class T>
class Buffer
{
public:
    Buffer();

    uint capacity() const;
    void setCapacity(int capacity);
    void setDrop(bool drop);

    void add(const T & frame);
    T take();

    const T & first() const;
    uint size() const;

    void clear();
    void reset();

    int dropCount;
    int encodedCount;

    bool isEmpty() const;

private:
    QQueue<T> m_buffer;
    QSemaphore m_usedSlot;
    QSemaphore m_freeSlot;
    int m_capacity;
    bool m_isDrop;
};

template<class T>
Buffer<T>::Buffer()
{
    m_capacity = 0;
    m_isDrop = false;
    dropCount = 0;
    encodedCount = 0;
}

template<class T>
void Buffer<T>::setCapacity(int capacity)
{
    m_capacity = capacity;

    if(capacity > m_freeSlot.available())
        m_freeSlot.release(capacity - m_freeSlot.available());
    else if(capacity < m_freeSlot.available())
        m_freeSlot.tryAcquire(m_freeSlot.available() - capacity);
}

template<class T>
void Buffer<T>::setDrop(bool isDrop)
{
    m_isDrop = isDrop;
}

template<class T>
void Buffer<T>::add(const T & frame)
{
    if(m_isDrop)
    {
        if(m_freeSlot.tryAcquire())
        {
            // Append frame data
            m_buffer.enqueue(frame);
            m_usedSlot.release();
        }
        // Buffer is too big, drop frames
        else
        {
            // Append frame and drop first
            m_buffer.enqueue(frame);
            m_buffer.removeFirst();

            // Debug drops every lost second
            dropCount++;
            if(dropCount % 10 == 0)
                qDebug() << "WARNING" << "Buffer is full. Element dropped. Total drops" << dropCount;
        }
    }
    else
    {
        // Append after waiting
        if(m_freeSlot.tryAcquire(1, 1000. / MIN_FPS))
        {
            m_usedSlot.release();
            m_buffer.enqueue(frame);
        }
    }
}

template<class T>
T Buffer<T>::take()
{
    T data;
    data.isValid = false;

    if(m_usedSlot.tryAcquire())
    {
        data = m_buffer.dequeue();
        m_freeSlot.release();

        return data;
    }
    else {
        return data;
    }
}

template<class T>
const T & Buffer<T>::first() const
{
    return m_buffer.first();
}

template<class T>
void Buffer<T>::clear()
{
    if(m_buffer.size() != 0)
    {
        if(m_capacity > m_freeSlot.available())
            m_freeSlot.release(m_capacity - m_freeSlot.available());
        else if(m_capacity < m_freeSlot.available())
            m_freeSlot.tryAcquire(m_freeSlot.available() - m_capacity);
        m_usedSlot.tryAcquire(m_buffer.size());

        m_buffer.clear();

        dropCount = 0;
        encodedCount = 0;
    }
}

template<class T>
void Buffer<T>::reset()
{
    dropCount = 0;
    encodedCount = 0;
}

template<class T>
bool Buffer<T>::isEmpty() const
{
    return m_buffer.isEmpty();
}

template<class T>
uint Buffer<T>::size() const
{
    return m_buffer.size();
}

template<class T>
uint Buffer<T>::capacity() const
{
    return m_capacity;
}

#endif // BUFFER_H
