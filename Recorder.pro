#VideoBooth Project

QT += core widgets network
TARGET = Recorder
TEMPLATE = app

HEADERS += apihandler.h faceengine.h filterlist.h keyboard.h buffer.h faceprocessing.h focusedit.h keypushbutton.h capture.h filter.h form.h scene.h
SOURCES += apihandler.cpp faceprocessing.cpp focusedit.cpp keypushbutton.cpp capture.cpp filter.cpp form.cpp faceengine.cpp filterlist.cpp keyboard.cpp scene.cpp main.cpp

FORMS  += form.ui

QMAKE_CXXFLAGS += -std=c++11

CONFIG += link_pkgconfig
PKGCONFIG += libv4l2 # sudo apt-get install libv4l-dev

RESOURCES += resources.qrc

LIBS +=  -ldlib -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_objdetect -lopencv_videoio -lopencv_dnn -lopencv_imgcodecs -lopencv_calib3d -lopencv_flann -lopencv_features2d
LIBS += -L$$PWD/dependencies/seetafacedetection/lib -lseeta_facedet_lib
LIBS += -lopencv_tracking #build the opencv_contrib modules to get tracking

 
INCLUDEPATH += $$PWD/dependencies/seetafacedetection/include
QMAKE_RPATHDIR += $$OUT_PWD/dependencies/seetafacedetection/lib
               
copydata.commands = $(MKDIR) $$OUT_PWD/dependencies && $(COPY_DIR) $$PWD/dependencies/* $$OUT_PWD/dependencies

first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
