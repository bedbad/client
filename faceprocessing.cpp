#include "faceprocessing.h"
#include <QDebug>
#include <QTime>

#include <cmath>
#include <fstream>

using namespace std;
using namespace cv;

const char* trackerAlgorithm = "MEDIANFLOW";

float test_data[18] = {  //Name:                     |Predictor Part Number:
     0.0f,    0.0f,    0.0f,        //Nose tip                  (30)
     0.0f,   -330.0f, -65.0f,       //Chin                      (8)
    -225.0f,  170.0f, -135.0f,      //Left eye, left corner     (36)
     225.0f,  170.0f, -135.0f,      //Right eye, right corner   (45)
    -150.0f, -150.0f, -125.0f,      //Left Mouth Corner         (48)
     150.0f, -150.0f, -125.0f       //Right Mouth Corner        (54)
};

ModelData::ModelData(std::string path){
    load(path);
}

void ModelData::load(std::string path){
    ifstream fin(path);

    while(!fin.eof()){
        data.push_back(Point3f());
        fin >> data.rbegin()->x >> data.rbegin()->y >> data.rbegin()->z;
    }

    fin.close();
}

cv::Point3f& ModelData::get(int idx){
    return data[idx];
}

vector<Point3f> ModelData::getKeyMat(){
    vector<Point3f> ret;

    for(uint i = 0; i < KEY_COUNT; i++){
        //ret.push_back(data[KEY_LANDMARKS[i]]);
        ret.push_back(Point3f(test_data[i*3],test_data[i*3+1],test_data[i*3+2]));
    }

    return ret;
}

Object::Object(){
    this->location = {0,0,0,0};
    this->confidence = -1;
    merged = false;
    updated = false;
    pyrScale = 1;
}

Object::Object(cv::Rect location, cv::Mat& img, double frameTime, float confidence, int pyrScale): keyTracker(nullptr){
    this->location = location;
    this->confidence = confidence;
    initTracker(img);
    merged = false;
    updated = false;
    this->pyrScale = pyrScale;
    initKalmanFilter(frameTime);
}

Rect2d estimateKeyChunk(cv::Rect2d bbox, cv::Point2d keypoint){
    double width = bbox.width * (CHUNK_SIZE / 100.);
    return Rect2d(keypoint.x - width / 2, keypoint.y - width / 2, width, width);
}

void Object::initTracker(Mat& img){
    if(!tracker.empty())tracker->clear();
    tracker = Tracker::create(trackerAlgorithm);
    tracker->init(img, location);
}

void Object::initKeyTracker(Mat& img){
    if(keyTracker != nullptr){
        delete keyTracker;
    }keyTracker = new MultiTracker(trackerAlgorithm);

    vector<Rect2d> chunks;

    for(uint i = 0; i < KEY_COUNT; i++){
        chunks.push_back(estimateKeyChunk(location,face.keypoints[i]));
    }

    keyTracker->add(img, chunks);
}

void Object::initKalmanFilter(double dt){
    double dt2 = (dt * dt) / 2;

    kFilter.init(18,6,0,CV_64F);

    setIdentity(kFilter.processNoiseCov, cv::Scalar::all(1e-5));
    setIdentity(kFilter.measurementNoiseCov, cv::Scalar::all(1e-4));
    setIdentity(kFilter.errorCovPost, cv::Scalar::all(1));

    kFilter.transitionMatrix.at<double>(0,3) = dt;
    kFilter.transitionMatrix.at<double>(1,4) = dt;
    kFilter.transitionMatrix.at<double>(2,5) = dt;
    kFilter.transitionMatrix.at<double>(3,6) = dt;
    kFilter.transitionMatrix.at<double>(4,7) = dt;
    kFilter.transitionMatrix.at<double>(5,8) = dt;
    kFilter.transitionMatrix.at<double>(0,6) = dt2;
    kFilter.transitionMatrix.at<double>(1,7) = dt2;
    kFilter.transitionMatrix.at<double>(2,8) = dt2;

    kFilter.transitionMatrix.at<double>(9,12) = dt;
    kFilter.transitionMatrix.at<double>(10,13) = dt;
    kFilter.transitionMatrix.at<double>(11,14) = dt;
    kFilter.transitionMatrix.at<double>(12,15) = dt;
    kFilter.transitionMatrix.at<double>(13,16) = dt;
    kFilter.transitionMatrix.at<double>(14,17) = dt;
    kFilter.transitionMatrix.at<double>(9,15) = dt2;
    kFilter.transitionMatrix.at<double>(10,16) = dt2;
    kFilter.transitionMatrix.at<double>(11,17) = dt2;

    kFilter.measurementMatrix.at<double>(0,0)= 1;   //x
    kFilter.measurementMatrix.at<double>(1,1)= 1;   //y
    kFilter.measurementMatrix.at<double>(2,2)= 1;   //z
    kFilter.measurementMatrix.at<double>(3,9)= 1;   //roll(around x axis)
    kFilter.measurementMatrix.at<double>(4,10)= 1;  //pitch(around y axis)
    kFilter.measurementMatrix.at<double>(5,11)= 1;  //yaw(around z axis)
}

void Object::correctRotation(){
    //face.rotation.at<double>(1) *= -1;
    if(face.rotation.at<double>(0) < 0){
        face.rotation.at<double>(0) += 2*CV_PI;
    }
}

void Object::updatePose(){
    correctRotation();

    Mat measured(6,1,CV_64F);
    measured.at<double>(0) = face.translation.at<double>(0);
    measured.at<double>(1) = face.translation.at<double>(1);
    measured.at<double>(2) = face.translation.at<double>(2);
    measured.at<double>(3) = face.rotation.at<double>(0);
    measured.at<double>(4) = face.rotation.at<double>(1);
    measured.at<double>(5) = face.rotation.at<double>(2);

    cv::Mat out = kFilter.predict();
    if(!err)out = kFilter.correct(measured);

    pose.translation.at<double>(0) = out.at<double>(0);
    pose.translation.at<double>(1) = out.at<double>(1);
    pose.translation.at<double>(2) = out.at<double>(2);
    pose.rotation.at<double>(0) = out.at<double>(9);
    pose.rotation.at<double>(1) = out.at<double>(10);
    pose.rotation.at<double>(2) = out.at<double>(11);
}

Point2d centerDelta(Rect2d& start, Rect2d& end){
    return Point2d(end.x - start.x + end.width / 2 - start.width / 2, end.y - start.y + end.height / 2 - start.height / 2);
}

bool Object::predict(FaceProcessor& faceProcessor, Mat& img){
    //if(updated)return true;

    vector<Rect2d> chunks;
    for(uint i = 0; i < KEY_COUNT; i++){
        chunks.push_back(estimateKeyChunk(location, face.keypoints[i]));
    }bool success = keyTracker->update(img);

    for(uint i = 0; i < KEY_COUNT; i++){
        face.keypoints[i] = Point2f(keyTracker->objects[i].x + keyTracker->objects[i].width / 2, keyTracker->objects[i].y + keyTracker->objects[i].height / 2);
    }

    faceProcessor.alignFace(face, img.cols, img.rows);

    confidence -= CONFIDENCE_DELTA;
    return success && confidence > 0;
}

cv::Rect2d Object::getLocation(){
    return {location.x*pyrScale, location.y*pyrScale,location.width*pyrScale,location.height*pyrScale};
}

void Object::update(Mat& img, Object newObj){
    location = newObj.location;
    confidence = newObj.confidence + CONFIDENCE_MERGE_BOOST;

    initTracker(img);

    updated = true;
}

bool Object::checkIntersect(Object& other){
    return (abs(other.location.x - location.x) * 2 < (other.location.width + location.width)) &&
            (abs(other.location.y - location.y) * 2 < (other.location.height + location.height));
}

FaceProcessor::FaceProcessor(string detector_path, string predictor_path, string model_path, double frameTime):
    faceDetector(detector_path.c_str()), facePredictor(), faceModelData(model_path){
    //faceDetector.SetImagexScaleFactor(4);
    dlib::deserialize(predictor_path) >> facePredictor;
    this->frameTime = frameTime;
    clahe = createCLAHE();
}

void FaceProcessor::pyramid(Mat& source, Mat& dest, int pyrLog){
    cvtColor(source, dest, COLOR_BGR2GRAY);

    if(pyrLog == 0)return;

    for(int i = 0; i < pyrLog; i++){
        pyrDown(dest,dest);
    }

    clahe->apply(dest, dest);
}

void FaceProcessor::detectFaces(cv::Mat& scaled, std::vector<Object>& output, int pyrScale){
    seeta::ImageData data(scaled.cols, scaled.rows);
    data.data = scaled.data;
    vector<seeta::FaceInfo> faces = faceDetector.Detect(data);

    for(seeta::FaceInfo face : faces){
        output.push_back(Object({face.bbox.x,face.bbox.y,face.bbox.width,face.bbox.height},scaled,frameTime,face.score,pyrScale));
    }
}

void Object::updateFace(FaceProcessor& faceProcessor, Mat& image){
    dlib::rectangle rect(location.x, location.y, location.x + location.width, location.y + location.height);
    face = faceProcessor.findFace(image, rect);

    initKeyTracker(image);
}

Face FaceProcessor::findFace(Mat& image, dlib::rectangle& rect){
    dlib::cv_image<unsigned char> dlib_image(image);
    dlib::full_object_detection shape = facePredictor(dlib_image, rect);//find face landmarks

    Face output;

    for(uint i = 0; i < KEY_COUNT; i++)
        output.keypoints[i] = Point2f(shape.part(KEY_LANDMARKS[i]).x(), shape.part(KEY_LANDMARKS[i]).y());

    alignFace(output, image.cols, image.rows);

    return output;
}

void FaceProcessor::alignFace(Face& face, int width, int height){
    Point2f center(width/2, height/2);
    Mat camMatrix = (Mat_<float>(3,3) <<
            width, 0,     center.x,
            0,     width, center.y,
            0,     0,     1);

    static vector<Point3f> face_model = faceModelData();
    Mat source_model(KEY_COUNT, 1, CV_32FC2, face.keypoints);

    solvePnPRansac(face_model, source_model, camMatrix, Mat::zeros(4,1,CV_32F), face.rotation, face.translation);

    face.isValid = true;
}

void FaceProcessor::Update(cv::Mat& input, bool findNew, bool updateFacePoses, vector<Pose>& output){
    static int pyrLog = -1;
    static int pyrScale = 1;
    if(pyrLog < 0){
        pyrLog = log2(input.cols / DESIRED_RESOLUTION);
        pyrScale = 1u << pyrLog;
    }

    cerr << "before\n";
    cv::Mat scaled = input;
    pyramid(input,scaled,pyrLog);

    vector<Object> newFaces;
    cerr << findNew << endl;
    if(findNew)detectFaces(scaled, newFaces, pyrScale);

    for(int i = objects.size() - 1; i >= 0; i--){
            cerr<< "eye:" << objects[0].face.keypoints[0] << "\n";
        bool success = objects[i].predict(*this, scaled);


        for(Object& detected : newFaces){
            if(objects[i].checkIntersect(detected)){
                if(!detected.canMerge()){
                    success = false;
                }else{
                    objects[i].update(scaled, detected);

                    detected.merge();
                    success = true;
                }
            }

        }


        //if(!success)objects.erase(objects.begin() + i);
    }

    for(Object& face : newFaces){
        if(face.canMerge()){
            objects.push_back(face);
        }
    }

    for(Object& face : objects){
        if(updateFacePoses || !face.hasFace())face.updateFace(*this, scaled);
        face.updatePose();

        output.push_back(face.getPose());
    }i++;
}

Point2i constrain(Point2i n, Point2i max){
    Point2i m = n;
    if(m.x < 0)m.x = 0;
    else if(m.x > max.x)m.x = max.x;
    if(m.y < 0)m.y = 0;
    else if(m.y > max.y)m.y = max.y;
    return m;
}

void FaceProcessor::ApplyFilter(Mat& frame, Pose& pose, Filter* filter, int index = 0){
    if(filter == nullptr){
        return;
    }



    Point2f center(frame.cols/2, frame.rows/2);
    double f = frame.cols;//approximate focal length with image width

    Mat camMatrix = (Mat_<float>(3,3) <<
            f, 0, center.x,
            0, f, center.y,
            0, 0, 1);

    vector<Point2f> points2D;
    projectPoints(filter->getCoordinates(), pose.rotation, pose.translation, camMatrix, Mat::zeros(4,1,CV_32F), points2D);

    Point2i min = points2D[0];
    Point2i max = points2D[0];
    for(uint i = 1; i < points2D.size(); i++){
        if(points2D[i].x < min.x)min.x = points2D[i].x;
        else if(points2D[i].x > max.x)max.x = points2D[i].x;
        if(points2D[i].y < min.y)min.y = points2D[i].y;
        else if(points2D[i].y > max.y)max.y = points2D[i].y;
    }

    for(Point2f& point : points2D){
        point.x -= min.x;
        point.y -= min.y;
    }

    vector<Point2f> frameCorners;
    frameCorners.push_back(Point2f(0,0));
    frameCorners.push_back(Point2f(0,filter->getGraphic(index).rows - 1));
    frameCorners.push_back(Point2f(filter->getGraphic(index).cols - 1,filter->getGraphic(index).rows - 1));
    frameCorners.push_back(Point2f(filter->getGraphic(index).cols - 1,0));

    //find the mapping between the filter graphic and its position on the face
    Mat H = findHomography(frameCorners, points2D);

    //change the filter's perspective to the final position
    Mat filterWarped;
    warpPerspective(filter->getGraphic(index), filterWarped, H, Size(max - min));

    min = constrain(min,Point2i(frame.cols-1,frame.rows-1));
    max = constrain(max,Point2i(frame.cols-1,frame.rows-1));

    if(min == max)return;

    Rect2i bounds(min,max);

    if(bounds.width > filterWarped.cols)bounds.width = filterWarped.cols;
    if(bounds.height > filterWarped.rows)bounds.height = filterWarped.rows;

    Mat cropped = filterWarped(Rect(0,0,bounds.width,bounds.height));

    //apply the filter to the frame
    Mat alpha(cropped.rows,cropped.cols,CV_8UC1);
    Mat filterFinal(cropped.rows,cropped.cols,CV_8UC3);
    int fromTo[8] = {0,0,1,1,2,2,3,3};
    Mat mix[2] = {filterFinal, alpha};
    mixChannels(&cropped, 1, mix, 2, fromTo, 4);

    Mat mask;
    threshold(alpha, mask, 127, 255, THRESH_BINARY);//only copy the parts of the filter with greater than 50% opacity (TODO: proper alpha blending)

    filterFinal.copyTo(frame(bounds), mask);
}
